<?php
//Claim sorting and structuring program
//Keith Neuendorff coding test


//Read in claims file to array
$linesRawArray = file( 'claims.txt');
$linesArraySize = sizeof($linesRawArray);

//Get rid of double quotes and beginning and end of file
$linesRawArray[0] = substr($linesRawArray[0],1);
$linesRawArray[$linesArraySize-1] = substr($linesRawArray[$linesArraySize-1],0,-1);

//Create array of objects to work on in program
//trim trailing new lines and white space
$linesArray = Array();
for ($i=0;$i<$linesArraySize;$i++) {
    $linesArray[] = (object)['text' => rtrim((string)($linesRawArray[$i]))];
}

//Process all the elaboration lines - splice out and place under parent
$curClaimIndex = -1;
$curLastElaborationIndex = -1;
$foundPrimaryClaim = false;
$foundNextChildClaim = false;
for ($i=0;$i<$linesArraySize;$i++) {
    if ($foundPrimaryClaim) {
        if (isPrimary($linesArray[$i]->text) || isChild($linesArray[$i]->text)) {
            $curLastElaborationIndex = $i-1;
            $foundNextChildClaim = true;
        }
    }
    else {
        if (isPrimary($linesArray[$i]->text)) {
            $foundPrimaryClaim = true;
            $curClaimIndex = $i;
        }
    }

    if ($foundNextChildClaim && $foundPrimaryClaim) {
        if ($curLastElaborationIndex <= $curClaimIndex) {
        }
        else {
            for($j=$curClaimIndex+1;$j<=$curLastElaborationIndex;$j++) {
                //Place the elaborations under the primary claim
                $linesArray[$j]->isElaboration = true;
                $linesArray[$j]->elabParentIndex = $curClaimIndex;
            }
        }
        $foundPrimaryClaim = true;
        $foundNextChildClaim = false;
        $curClaimIndex = $curLastElaborationIndex+1;
    }
}   //for $i

//Move items into elab parents
for ($i=0;$i<$linesArraySize;$i++) {
    if($linesArray[$i]->isElaboration) {
        if(!isset($linesArray[$linesArray[$i]->elabParentIndex]->elaboration)) {
            $linesArray[$linesArray[$i]->elabParentIndex]->elaboration = array();
        }
        array_push($linesArray[$linesArray[$i]->elabParentIndex]->elaboration, $linesArray[$i]->text);
    }    
}

//Remove elaborations from main array starting at end
for ($i=$linesArraySize-1;$i>=0;$i--) {
    if($linesArray[$i]->isElaboration) {
        array_splice( $linesArray, $i, 1 );
    }
}

//Process all the child claims
//process all then unsplice as needed from top array
$adjustedArraySize = sizeof($linesArray);
for ($i=0;$i<$adjustedArraySize;$i++) {
    $numberArray = getInternalRefNumber( $linesArray[$i]->text );
    $linesArray[$i]->lineNumber = $numberArray[0];
    if(isChild($linesArray[$i]->text)) {
        $linesArray[$i]->refNumber = $numberArray[1];
        $linesArray[$i]->isChild = true;
    }
}

//Move child items under their parent
for ($i=0;$i<$adjustedArraySize;$i++) {
    if($linesArray[$i]->isChild) {
        //then move the child claim
        //back search to get to parent match sooner
        $curRefNumber = $linesArray[$i]->refNumber;
        for($j=$i;$j>=0;$j--) {
            if($linesArray[$j]->lineNumber == $curRefNumber) {
                if(!isset($linesArray[$j]->childClaims)) {
                    $linesArray[$j]->childClaims = array();
                }
                array_push($linesArray[$j]->childClaims, $linesArray[$i]);
                break;   //out of inner for
            }
        }
    }    
}

//Remove the references to child claims from main array - start at end
for ($i=$linesArraySize-1;$i>=0;$i--) {
    if($linesArray[$i]->isChild) {
        array_splice( $linesArray, $i, 1 );
    }
}

//Print out the JSON result
$claimsJson = json_encode($linesArray);
//$claimsJson = json_encode($linesArray, JSON_PRETTY_PRINT);   //for debugging

echo $claimsJson;


//Utility functions
function isPrimary( $claimString ) {
    $result = preg_match("/^[0-9]+\..*:/", $claimString);
    return $result;
}

function isChild( $claimString ) {
    $result = preg_match("/^[0-9]+\. The.* of claim [0-9]+,.*\./", $claimString);
    return $result;
}

function getInternalRefNumber( $claimString ) {
    preg_match_all('!\d+!', $claimString, $matches);
    $claimNumber = $matches[0][0];
    $refNumber = $matches[0][1];
    return array($claimNumber, $refNumber);
}

?>
